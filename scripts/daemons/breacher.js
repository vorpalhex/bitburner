import { registerCommand, readPort } from "/lib/network";

const invalidTargets = ["home"];
const portNumber = 1;

/** @param {NS} ns **/
export async function main(ns) {
	let alive = true;
	ns.disableLog("sleep")
	registerCommand(ns, "breach", breach, 1);
	while(alive) {
		await readPort(ns, portNumber);
		await ns.sleep(50);
	}
}

/** @param {NS} ns **/
export async function breach(ns, target) {
	ns.toast(`Breaching ${target}`);
	ns.print('checking valid target');
	if (!isValidTarget(ns, target)) return ns.print(`${target} is an invalid target`);

	ns.print('checking existing access');
	if (ns.hasRootAccess(target)) {
		//nothing to do
		ns.print(`${target} is already breached`);
		return
	}

	ns.print('breaching ports');
	const breachSuccess = await breachPorts(ns, target);
	if ( !breachSuccess) return  ns.toast(`failed to breach ports of ${target}`, "warning");

	ns.print('gaining root');
	ns.nuke(target);
	ns.print(`Nuked ${target}`)
}

/** @param {NS} ns **/
export function hasProgram(ns, program) {
	return true;
	// ns.print("getting programs");
	// apps = []; //ns.ls("home");
	// ns.print("apps on host", apps);
	// renamedPrograms = apps.map( (program) => program.toLower().replace(".exe", "") );
	// ns.print("renamed programs", renamedPrograms);
	// if (renamedPrograms.includes(program.toLower())) return true;
	// return false;
}

/** @param {NS} ns **/
export async function breachPorts(ns, host) {
	await ns.sleep(10);
	// const potentialPortBreakers = [
	// 	"brutessh",
	// 	"ftpcrack",
	// 	"relaysmtp",
	// 	"sqlinject",
	// 	"httpworm"
	// ];
	const portBreakers = [
		ns.brutessh,
		ns.ftpcrack,
		ns.relaysmtp,
		ns.sqlinject,
		ns.httpworm
	];

	ns.print("Available port breakers", portBreakers);
	let ports = ns.getServerNumPortsRequired(host);
	if ( ports == 0 ) return true; //exit early
	if (ports > portBreakers.length) return false;

	// for ( let i = 0; i < ports; i++ ) {
	// 	ns.print(`Trying ${breaker} port breaker`);
	// 	try {
	// 		await portBreakers[i](host);
	// 	} catch (e) {
	// 		ns.print(e);
	// 	}
	// }

	ns.brutessh(host);
	ns.ftpcrack(host);
	ns.relaysmtp(host);
	ns.sqlinject(host);
	ns.httpworm(host);

	return true;
}

/** @param {NS} ns **/
export function isValidTarget(ns, host) {
	if ( invalidTargets.includes(host) ) return false;

	const playerServers = ns.getPurchasedServers();
	if ( playerServers.includes(host) ) return false;

	const difficulty = ns.getServerSecurityLevel(host);
	if ( difficulty > ns.getPlayer().hacking ) return false;

	return true;
}