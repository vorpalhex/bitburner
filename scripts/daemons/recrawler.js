const infector = "crawler.js"
/** @param {NS} ns **/
export async function main(ns) {
	let alive = true;

	while(alive) {
		await ns.sleep(5 * 60 * 1000);
    ns.exec(infector, "home", 1);
	}
}