import { registerCommand, readPort } from "/lib/network";

const targetList = [];
const portNumber = 2;

/** @param {NS} ns **/
export async function main(ns) {
	let alive = true;
  registerCommand(ns, "addTarget", addTarget);
	while(alive) {
    await readPort(ns, 2);
		await ns.sleep(500);
	}
}

export async function addTarget(ns, target) {
  if (targetList.includes(target)) return
  targetList.push(target);
  await ns.sleep(10);
}