const invalidTargets = ["home"];
let visitedTargets = [];

const breachPort = 1;
const infector = "crawler.js"

/** @param {NS} ns **/
export async function main(ns) {
	if ( ns.args.length > 0 ) { //if called with any args, reinfect from the start
		visitedTargets = [];
	}
	let hosts = ns.scan();
	let localHost = ns.getHostname();
	hosts = hosts.filter((host) => isValidTarget(ns, host)); //only breach new hosts

	for (let i = 0; i < hosts.length; i++) {
		try {
			await doInfect(ns, hosts[i]);
			visitedTargets.push(hosts[i]);
		} catch (e) {
			ns.toast(`Error breaching host ${hosts[i]}: ${e}`, "warning", 5000);
		}
	}

	await ns.sleep(1000);
}

/** @param {NS} ns **/
async function doInfect(ns, host) {

	const presentHost = ns.getHostname();
	await ns.scp(infector, presentHost, host);

	//handle script updates
	ns.scriptKill(infector, host);

	if (!ns.hasRootAccess(host)) {
		ns.toast(`requesting breach ${host}`);
    await writePort(ns, breachPort, "breach", [host]);
    await ns.sleep(2000);
	}

	ns.exec(infector, host, 1);
	await ns.sleep(1000); //safety sleep
	return true;
}

/** @param {NS} ns **/
export function isValidTarget(ns, host) {
	if ( invalidTargets.includes(host) ) return false;
  if ( visitedTargets.includes(host) ) return false;

	const playerServers = ns.getPurchasedServers();
	if ( playerServers.includes(host) ) return false;

	const difficulty = ns.getServerSecurityLevel(host);
	if ( difficulty > ns.getPlayer().hacking ) return false;

	return true;
}

/** @param {NS} ns **/
export async function writePort(ns, portNumber, cmd, args=[]) {
  const payload = { cmd, args, version: 1 };
  const payloadString = JSON.stringify(payload);
  await ns.writePort(portNumber, payloadString);
}