import {writePort} from "/lib/network.js"
const port = 1;

/** @param {NS} ns **/
export async function main(ns) {
  const target = ns.args[0];
  if (!target) return ns.toast("Missing host. Form of `breach {host}`.");
  writePort(ns, "breach", [target]);
}