/** @param {NS} ns **/
export async function main(ns) {
	const host = ns.args[0];
	const maxMoney = ns.getServerMaxMoney(host);
	const minSecurity = ns.getServerMinSecurityLevel(host);
	const threads = ns.threads;

	const moneyTarget = 0.20 * maxMoney;

	if (!ns.hasRootAccess(host)) {
		alert(`Target ${host} not breached!`);
	}

	let doHack = true;
	let doWeaken = false;
	let doGrow = false;

	while (doHack) {
		doWeaken = false;
		doGrow = false;

		if (ns.hackAnalyzeChance(host) < 0.50 && ns.getServerSecurityLevel(host) > minSecurity) {
			doWeaken = true;
		}

		if (!doWeaken && ns.getServerMoneyAvailable(host) < moneyTarget) {
			doGrow = true;
		}

		if (doWeaken) await ns.weaken(host, { threads: threads });

		if (doGrow) await ns.grow(host, { threads: threads });

		await ns.hack(host, { threads: threads });
	}
}