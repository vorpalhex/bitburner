/** @param {NS} ns **/
export async function main(ns) {
	if (ns.args.length < 2) {
		print("Not enough args. Need {ram} and {count}");
		return;
	}

	const ram = ns.args[0];
	const count = ns.args[1];
	const pricePerNode = ns.getPurchasedServerCost(ram);
	if ( pricePerNode * count > ns.getPlayer().money) {
		ns.alert(`requested server setup of ${count} servers with ${ram}gb is ${pricePerNode * count} - more than our current money`);
	}
	for(let i = 0; i < count; i++) {
		ns.purchaseServer(`task${i}`, ram);
	}
}