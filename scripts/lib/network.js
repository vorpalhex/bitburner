const commands = {};
const version =  1;

/** @param {NS} ns **/
export function registerCommand(ns, cmd, fn, minArgs=0) {
  commands[cmd] = { fn, minArgs };
}

/** @param {NS} ns **/
export async function readPort(ns, portNumber) {
	const payload = ns.readPort(portNumber);
	if ( payload === "NULL PORT DATA" ) return;

	let parsedPayload = null;
	try {
		parsedPayload = JSON.parse(payload);
	} catch (e) {
		ns.print(`Received bad payload: ${payload}`);
		return
	}

	if (!parsedPayload || !parsedPayload.cmd || !parsedPayload.args || !parsedPayload.version ) return ns.print(`Received badly formatted payload: ${payload}`);
  if (parsedPayload.version != version) return ns.print(`Version mismatch. Expected ${version} but got ${payload.version}`);
  if (!Object.keys(commands).includes(parsedPayload.cmd)) return ns.print(`Invalid Command: ${parsedPayload.cmd}`);

	ns.print(`Received command: ${parsedPayload.cmd}`);
	ns.print(`Going to run ${commands[parsedPayload.cmd]}`);
	try {
		await commands[parsedPayload.cmd].fn(ns, ...parsedPayload.args);
	} catch (e) {
		ns.print(`error`, e, parsedPayload.cmd, parsedPayload.args);
	}
}

/** @param {NS} ns **/
export async function writePort(ns, portNumber, cmd, args=[]) {
  const payload = { cmd, args, version };
  const payloadString = JSON.stringify(payload);
  ns.writePort(portNumber, payload);
}