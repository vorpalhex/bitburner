const avoidTargets = ["home", "task1", "task2", "task3", "task4", "task5"];
let visitedTargets = [];

const skill = 100;
const portBreakers = 0;

const malware = "autobreach.js";
const infector = "autoinfect.js";

/** @param {NS} ns **/
export async function main(ns) {
	if ( ns.args.length > 0 ) {
		visitedTargets = [];
	}
	let hosts = ns.scan();
	let localHost = ns.getHostname();
	let deniedHosts = visitedTargets.concat(avoidTargets);
	hosts = hosts.filter((host) => !deniedHosts.includes(host)); //only breach new hosts

	for (let i = 0; i < hosts.length; i++) {
		try {
			await doInfect(ns, hosts[i]);
			visitedTargets.push(hosts[i]);
		} catch (e) {
			ns.print(`Error breaching host ${hosts[i]}: ${e}`);
			alert(e);
			await ns.sleep(5000); //give us a chance to read log
			ns.exit();
		}
	}
	if (!deniedHosts.includes(localHost)) {
		ns.spawn(malware, 2, localHost);
	}
	await ns.sleep(5000);
}

/** @param {NS} ns **/
async function doInfect(ns, host) {
	//make sure we can actually breach the host
	const hostDifficulty = ns.getServerRequiredHackingLevel(host);
	if (hostDifficulty > ns.getPlayer().hacking) {
		ns.print(`Host ${host} is too difficult, skipping...`);
		return
	}

	const portBreakers = [
		// ns.brutessh,
		// ns.ftpcrack,
		// ns.relaysmtp,
		// ns.sqlinject,
		// ns.httpworm
	];

	const targetRam = ns.getServerMaxRam(host);
	const payloadRam = ns.getScriptRam(malware, host);
	const infectRam = ns.getScriptRam(infector, host);
	const minimumRam = payloadRam + infectRam;
	if (targetRam < infectRam) {
		ns.print(`Not breaching ${host} as it only has ${targetRam} ram.`);
		return;
	}

	const presentHost = ns.getHostname();
	await ns.scp(infector, presentHost, host);
	await ns.scp(malware, presentHost, host);

	//handle script updates
	ns.scriptKill(infector, host);
	ns.scriptKill(malware, host);

	if (!ns.hasRootAccess(host)) {
		let ports = ns.getServerNumPortsRequired(host);
		if (ports > portBreakers) {
			ns.print(`Host ${host} can't be broken right now`);
			return false;
		}
		ns.print(`Breaching ${host}...`);
		for (let i = 0; i < ports; i++) {
			if (i > portBreakers.length || !portBreakers[i]) {
				ns.print(`Missing port breaker`);
				return false;
			}
			portBreakers[i](host);
		}
		ns.nuke(host)
		if (!ns.hasRootAccess(host)) {
			ns.print(`Can not breach ${host}`);
			return false;
		}
	}

	let targetThreads = Math.floor((targetRam - infectRam) / payloadRam);
	if (targetThreads > 0) ns.exec(malware, host, targetThreads, host);
	ns.exec(infector, host, 1);
	await ns.sleep(1000); //safety sleep
	return true;
}